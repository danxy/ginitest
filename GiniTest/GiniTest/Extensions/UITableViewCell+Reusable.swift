//
//  UITableViewCell+Reusable.swift
//  GlobalbitTestApp
//
//  Created by Daniel Nesterenko on 03/10/2018.
//  Copyright © 2018 Daniel Broyde. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionViewCell: Reusable {}

protocol Reusable: class {
    static var nib: UINib { get }
    static var reuseIdentifier: String { get }
}

extension Reusable {
    static var nib: UINib {
        return UINib(nibName: String(describing: Self.self), bundle: nil)
    }
    
    static var reuseIdentifier: String {
        return String(describing: Self.self) + "Identifier"
    }
    
    static var cellClass: AnyClass {
        return Self.self
    }
}
