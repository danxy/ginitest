//
//  NumbersModel.swift
//  GiniTest
//
//  Created by Daniel Nesterenko on 03.08.2020.
//  Copyright © 2020 Db. All rights reserved.
//

import Foundation

struct NumbersData: Codable {
    let numbers: [Number]
}

struct Number: Codable {
    let number: Int
}
