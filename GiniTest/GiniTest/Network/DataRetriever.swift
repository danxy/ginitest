//
//  DataRetriever.swift
//  GiniTest
//
//  Created by Daniel Nesterenko on 03.08.2020.
//  Copyright © 2020 Db. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire

class DataRetriever {
    static let shared = DataRetriever()
    
    private init(){}
    
    public func getNumbers<T: Codable>(completionHandler: @escaping (T?, Error?)->Void) {
        
        request("https://pastebin.com/raw/8wJzytQX", method: .get).responseDecodableObject { (response: DataResponse<T>)  in
            switch response.result {
            case .success(let value):
                completionHandler(value, nil)
            case .failure(let error):
                completionHandler(nil, error)
            }
        }
    
    }
}
