//
//  MainViewControllerPresenter.swift
//  GiniTest
//
//  Created by Daniel Nesterenko on 03.08.2020.
//  Copyright © 2020 Db. All rights reserved.
//

import Foundation

protocol MainViewControllerProtocol {
    func recievedNumbers()
}

class MainViewControllerPresenter {

    let view: MainViewControllerProtocol
    var data: [CellData] = []
    
    init(view: MainViewControllerProtocol) {
        self.view = view
    }
    
    private func setupCollectionViewData(_ numbers: [Number]) {
        var dic = Dictionary<Int, CellData>()
        var set = Set<Int>()
        
        numbers.forEach { (number) in
            let num = number.number
            let isInserted = set.insert(abs(num)).inserted
            if !isInserted {
                dic[num] = CellData(number: num, type: .big)
                dic[-num] = CellData(number: num, type: .big)
            } else {
                dic[num] = CellData(number: num, type: .small)
            }
        }
        data.append(contentsOf: dic.values)
//        numbers.forEach { (number) in
//            let num = number.number
//            if (dic[num] != nil) || (dic[-num] != nil) {
//                dic[num] = CellData(number: num, type: .big)
//                dic[-num] = CellData(number: -num, type: .big)
//            } else {
//                dic[num] = CellData(number: num, type: .small)
//            }
//        }
//        data.append(contentsOf: dic.values)
    }
    
    public func getNumbers() {
        DataRetriever.shared.getNumbers { (numbersData: NumbersData?, error) in
            guard let numbers = numbersData?.numbers else {return}
            self.setupCollectionViewData(numbers)
            self.view.recievedNumbers()
        }
    }
    
}
