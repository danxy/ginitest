//
//  MainViewController.swift
//  GiniTest
//
//  Created by Daniel Nesterenko on 03.08.2020.
//  Copyright © 2020 Db. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var presenter: MainViewControllerPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        setupPresenter()
        presenter?.getNumbers()
    }
    
    private func registerCells() {
        collectionView.register(SmallCollectionViewCell.nib, forCellWithReuseIdentifier: SmallCollectionViewCell.reuseIdentifier)
        collectionView.register(BigCollectionViewCell.nib, forCellWithReuseIdentifier: BigCollectionViewCell.reuseIdentifier)
    }
    
    private func setupPresenter() {
        presenter = MainViewControllerPresenter(view: self)
    }
    
}

extension MainViewController: MainViewControllerProtocol {
    func recievedNumbers() {
        self.collectionView.reloadData()
    }
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.data.count ?? 0
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: CellDataSetable?
        switch presenter!.data[indexPath.row].type {
        case .big:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: BigCollectionViewCell.reuseIdentifier, for: indexPath) as? CellDataSetable
            cell?.setupCellData(presenter!.data[indexPath.row])
            return (cell as? BigCollectionViewCell) ?? UICollectionViewCell()
        case .small:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: SmallCollectionViewCell.reuseIdentifier, for: indexPath) as? CellDataSetable
            cell?.setupCellData(presenter!.data[indexPath.row])
            return (cell as? SmallCollectionViewCell) ?? UICollectionViewCell()
        }

    }
}

extension MainViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellModel = presenter?.data[indexPath.row]
        return CGSize(width: UIScreen.main.bounds.width / 4, height: cellModel?.type == .small ? 50 : 100)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
  
}
