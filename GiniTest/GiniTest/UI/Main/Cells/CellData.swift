//
//  CellModel.swift
//  GiniTest
//
//  Created by Daniel Nesterenko on 03.08.2020.
//  Copyright © 2020 Db. All rights reserved.
//

import Foundation

enum CellType {
    case small
    case big
}

struct CellData {
    let number: Int
    let type: CellType
}

protocol CellDataSetable {
    func setupCellData(_ cellData: CellData)
    var cellType: CellType {get}
}
