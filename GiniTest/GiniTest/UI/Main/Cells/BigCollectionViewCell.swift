//
//  BigCollectionViewCell.swift
//  GiniTest
//
//  Created by Daniel Nesterenko on 03.08.2020.
//  Copyright © 2020 Db. All rights reserved.
//

import UIKit

class BigCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var valueLabel: UILabel!
    
    var data: CellData? {
        didSet {
            updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateUI() {
           self.valueLabel.text = data?.number.description
    }
    
    var cellType: CellType {
        return data?.type ?? .small
    }
}

extension BigCollectionViewCell: CellDataSetable {
    func setupCellData(_ cellData: CellData) {
        data = cellData
    }
}
